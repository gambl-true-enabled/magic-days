package com.genutey.fentuko.utils

import android.content.Context
import android.webkit.JavascriptInterface

private const val DAYS_TABLE = "com.DAYS.table.123"
private const val DAYS_ARGS = "com.DAYS.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(DAYS_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(DAYS_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(DAYS_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(DAYS_ARGS, null)
}


fun getMonthByNumber(number: Int): String? {
    val month = listOf(
        "Января", "Февраля", "Марта", "Апреля", "Мая", "Июня", "Июля", "Августа", "Сентября"
        , "Октября", "Ноября", "Декабря"
    )
    return if (number !in 1..12) {
        null
    } else {
        month[number - 1]
    }
}

private const val BIRTHDAY_TABLE = "com.birthday.table.DEEP"
private const val BIRTHDAY_ARGS = "com.birthday.value.DEEP"

fun Context.link(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(BIRTHDAY_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(BIRTHDAY_ARGS, deepArgs).apply()
}

fun Context.args(): String? {
    val sharedPreferences = getSharedPreferences(BIRTHDAY_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(BIRTHDAY_ARGS, null)
}
