package com.genutey.fentuko.model

import android.annotation.SuppressLint
import android.content.Context
import com.genutey.fentuko.model.entity.EventContact

@SuppressLint("StaticFieldLeak")
object Repository {

    lateinit var context: Context

    fun initRepository(context: Context) {
        Repository.context = context
    }

    fun insertEventContacts(eventContact: List<EventContact>) {
        for (event in eventContact) {
            AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.insertEventContact(event)
        }
    }

    fun getAllEvents(): List<EventContact> {
        return AppDatabase.getAppDataBase(context)?.eventContactsDao()!!.getAllEvents()
    }

}