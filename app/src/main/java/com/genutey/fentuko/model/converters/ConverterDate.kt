package com.genutey.fentuko.model.converters

import androidx.room.TypeConverter
import java.util.*

class ConverterDate {
    @TypeConverter
    fun fromDate(date: Date): Long {
        return date.time
    }

    @TypeConverter
    fun toDate(dateLong: Long): Date {
        return Date(dateLong)
    }
}