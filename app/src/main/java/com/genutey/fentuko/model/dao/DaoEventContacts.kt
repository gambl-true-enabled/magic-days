package com.genutey.fentuko.model.dao

import androidx.room.*
import com.genutey.fentuko.model.entity.EventContact

@Dao
interface DaoEventContacts {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertEventContact(EventContact: EventContact)

    @Query("SELECT * FROM eventcontact")
    fun getAllEvents(): List<EventContact>

}


