package com.genutey.fentuko.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.genutey.fentuko.R
import com.genutey.fentuko.model.entity.EventContact
import com.genutey.fentuko.presenter.adapters.MagicDaysAdapter
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment.view.*
import java.text.SimpleDateFormat
import java.util.*

class MainFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    fun showEvents(eventSort: List<EventContact>) {
        progress_bar.visibility = View.GONE
        val simpleDate = SimpleDateFormat("dd.MM.yyyy")
        val magicsList =
            eventSort.map { MagicDay(it.photoUri, it.name.split(" ").first(), simpleDate.format(it.date), it.id) }
        Log.d("MAGIC_LIST", magicsList.toString())
        if (magicsList.isEmpty()) {
            text_empty.visibility = View.VISIBLE
            placeholder_empty.visibility = View.VISIBLE
            top_card.visibility = View.GONE
            grid_magic_days.visibility = View.GONE
        } else {
            val currentTime: Date = Calendar.getInstance().time
            val currMagicDay = magicsList.find { it.date == simpleDate.format(currentTime) }
            if (currMagicDay == null) {
                top_card.visibility = View.GONE
            } else {
                top_card.visibility = View.VISIBLE
                name_top.text = currMagicDay.name
                date_top.text = simpleDate.format(currentTime)
                Glide.with(context!!)
                    .load(currMagicDay.avatar)
                    .placeholder(R.drawable.ic_launcher_foreground)
                    .circleCrop()
                    .into(icon_top)
                top_card.setOnClickListener {
                    val intent = Intent(Intent.ACTION_VIEW)
                    val uri = Uri.withAppendedPath(
                        ContactsContract.Contacts.CONTENT_URI,
                        currMagicDay.id
                    )
                    intent.data = uri
                    context!!.startActivity(intent)
                }
            }
            grid_magic_days.visibility = View.VISIBLE
            text_empty.visibility = View.GONE
            placeholder_empty.visibility = View.GONE
            val adapter = MagicDaysAdapter(context!!, magicsList.toMutableList())
            grid_magic_days.adapter = adapter
            grid_magic_days.expanded = true
        }
    }

    data class MagicDay(
        val avatar: String,
        val name: String,
        val date: String,
        val id: String
    )
}