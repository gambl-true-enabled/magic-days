package com.genutey.fentuko.presenter

import android.content.ContentResolver
import android.provider.ContactsContract
import androidx.fragment.app.Fragment
import com.genutey.fentuko.model.entity.EventContact
import com.genutey.fentuko.model.Repository
import com.genutey.fentuko.view.MainFragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ContactsPresenter(private val mainFragment: MainFragment) {

    private suspend fun getEvents(contentResolver: ContentResolver): ArrayList<EventContact> =
        withContext(Dispatchers.IO) {
            val events = hashMapOf<String, EventContact>()
            val phones = hashMapOf<String, String>()

            val projection =
                arrayOf(ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME)
            val cur = contentResolver.query(
                ContactsContract.Contacts.CONTENT_URI, projection, null, null,
                null, null
            )

            while (cur!!.moveToNext()) {

                val contactId = cur.getString(cur.getColumnIndex(ContactsContract.Data._ID))
                val contactId2 = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID))

                val columns = arrayOf(
                    ContactsContract.CommonDataKinds.Event.START_DATE,
                    ContactsContract.CommonDataKinds.Event.TYPE,
                    ContactsContract.CommonDataKinds.Event.MIMETYPE,
                    ContactsContract.CommonDataKinds.Event.DISPLAY_NAME,
                    ContactsContract.CommonDataKinds.Event.PHOTO_URI
                )

                val where =
                    "((" + ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_BIRTHDAY + ")" + "or" +
                            "(" + ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_OTHER + ")" + "or" +
                            "(" + ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_CUSTOM + ")" + "or" +
                            "(" + ContactsContract.CommonDataKinds.Event.TYPE + "=" + ContactsContract.CommonDataKinds.Event.TYPE_ANNIVERSARY + "))" +
                            " and " + ContactsContract.CommonDataKinds.Event.MIMETYPE + " = '" + ContactsContract.CommonDataKinds.Event.CONTENT_ITEM_TYPE + "' and " + ContactsContract.Data.CONTACT_ID + " = " + contactId
                val selectionArgs: Array<String>? = null
                val sortOrder = ContactsContract.Contacts.DISPLAY_NAME

                val birthdayCur = contentResolver.query(
                    ContactsContract.Data.CONTENT_URI,
                    columns,
                    where,
                    selectionArgs,
                    sortOrder
                )
                if (birthdayCur!!.count > 0) {
                    while (birthdayCur.moveToNext()) {
                        val parseDate =
                            birthdayCur.getString(birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.START_DATE))
                        val name =
                            birthdayCur.getString(birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.DISPLAY_NAME))
                        var photoUri =
                            birthdayCur.getString(birthdayCur.getColumnIndex(ContactsContract.CommonDataKinds.Event.PHOTO_URI))
                        if (photoUri == null) photoUri = ""
                        val date =
                            SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(parseDate)!!
                        if (date.time > 0) {
                            events[contactId] =
                                EventContact(
                                    contactId,
                                    name,
                                    "День рождения!",
                                    date,
                                    photoUri,
                                    ""
                                )
                        }
                    }
                }
                birthdayCur.close()

                val phonesCur = contentResolver.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                    null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId2,
                    null,
                    null
                )
                while (phonesCur!!.moveToNext()) {
                    val number =
                        phonesCur.getString(phonesCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                    phones[contactId2] = number
                }
                phonesCur.close()
            }
            val result = ArrayList<EventContact>()
            for (key in phones.keys) {
                if (events.containsKey(key)) {
                    val event = events[key]!!
                    val phone = phones[key]!!
                    result.add(
                        EventContact(
                            key,
                            event.name,
                            event.eventName,
                            event.date,
                            event.photoUri,
                            phone
                        )
                    )
                }
            }
            cur.close()
            result
        }


    fun initContacts(contentResolver: ContentResolver) {
        GlobalScope.launch(Dispatchers.Main) {
            val dbEvent = withContext(Dispatchers.IO) { Repository.getAllEvents() }
            if (dbEvent.isNotEmpty())
                setUpEvents(dbEvent)
            val events = getEvents(contentResolver)
            setUpEvents(events)
            withContext(Dispatchers.IO) { Repository.insertEventContacts(events) }
        }
    }

    private suspend fun setUpEvents(events: List<EventContact>) {
        val calendar = Calendar.getInstance()
        val calendarTemp = Calendar.getInstance()
        val currentYear = calendar.get(Calendar.YEAR)
        val eventSort = withContext(Dispatchers.IO) {
            events.sortedBy {
                calendarTemp.time = it.date
                val month = calendarTemp.get(Calendar.MONTH)
                val day = calendarTemp.get(Calendar.DAY_OF_MONTH)
                calendarTemp.set(currentYear, month, day)
                calendarTemp.timeInMillis
            }
        }

        mainFragment.showEvents(eventSort)
    }

    companion object Constants {
        const val WEEK_DURATION = 31104000000L
        const val MONTH_DURATION = 31104000000L
    }

}