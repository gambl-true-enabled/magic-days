package com.genutey.fentuko.presenter.adapters

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.ContactsContract
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.genutey.fentuko.R
import com.genutey.fentuko.model.Repository.context
import com.genutey.fentuko.view.MainFragment

class MagicDaysAdapter(
    private val mContext: Context,
    private val magicDaysList: MutableList<MainFragment.MagicDay>
) : BaseAdapter() {

    override fun getCount(): Int {
        return magicDaysList.size
    }

    override fun getItem(position: Int): Any? {
        return magicDaysList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(
        position: Int,
        convertView: View?,
        parent: ViewGroup?
    ): View? {
        val grid = if (convertView == null) {
            val inflater =
                mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            inflater.inflate(R.layout.card_magic_day, parent, false)
        } else {
            convertView
        }
        val imageView = grid.findViewById<View>(R.id.avatar) as ImageView
        val textDateView = grid.findViewById<View>(R.id.date_text) as TextView
        val textNameView = grid.findViewById<View>(R.id.name_text) as TextView
        Glide.with(mContext)
            .load(magicDaysList[position].avatar)
            .placeholder(R.drawable.ic_launcher_foreground)
            .circleCrop()
            .into(imageView)
        textDateView.text = magicDaysList[position].date
        textNameView.text = magicDaysList[position].name

        grid.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW)
            val uri = Uri.withAppendedPath(
                ContactsContract.Contacts.CONTENT_URI,
                magicDaysList[position].id
            )
            intent.data = uri
            mContext.startActivity(intent)

        }
        return grid
    }
}
