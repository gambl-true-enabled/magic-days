package com.genutey.fentuko

import android.app.Application
import android.util.Log
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import com.facebook.FacebookSdk
import com.facebook.appevents.AppEventsLogger
import com.genutey.fentuko.model.Repository
import com.onesignal.OneSignal

class MyApplication : Application() {

    private val conversionListener: AppsFlyerConversionListener = object : AppsFlyerConversionListener {
        override fun onConversionDataSuccess(conversionData: Map<String, Any>) {
            conversionData.forEach {
                Log.i("Days1", "${it.key} ${it.value}")
            }
        }
        override fun onConversionDataFail(errorMessage: String) {
            Log.i("Days2", "onConversionDataFail $errorMessage")
        }
        override fun onAppOpenAttribution(attributionData: Map<String, String>) {
            attributionData.forEach {
                Log.i("Days3", "${it.key} ${it.value}")
            }
        }
        override fun onAttributionFailure(errorMessage: String) {
            Log.i("Days4", "onAttributionFailure $errorMessage")
        }
    }

    override fun onCreate() {
        super.onCreate()
        Repository.initRepository(this)
        FacebookSdk.setAutoInitEnabled(true)
        FacebookSdk.fullyInitialize()
        FacebookSdk.sdkInitialize(applicationContext)
        AppEventsLogger.activateApp(this)

        AppsFlyerLib.getInstance().init("jzvY5PTmYwVkSNoTnySxYj", conversionListener, this)
        AppsFlyerLib.getInstance().startTracking(this)

        // Logging set to help debug issues, remove before releasing your app.
        OneSignal.setLogLevel(OneSignal.LOG_LEVEL.VERBOSE, OneSignal.LOG_LEVEL.NONE)
        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
    }


}